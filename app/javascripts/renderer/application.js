import Electron from 'electron';
import $ from 'jquery';
import dribbble from './dribbble';

Electron.ipcRenderer.on('loaded' , function(event, data) {
  // On $(document).ready(fn)
  $(() => {
    const $app = $('#app');

    dribbble({el: $app, page: 1, delay: 15000});
  });
});
