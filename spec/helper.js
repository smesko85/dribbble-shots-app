var path = require('path');

module.exports = {
  appPath: function() {
    switch (process.platform) {
      case 'darwin':
        return path.join(__dirname, '..', '.tmp', 'DribbleShots-darwin-x64', 'DribbleShots.app', 'Contents', 'MacOS', 'DribbleShots');
      case 'linux':
        return path.join(__dirname, '..', '.tmp', 'DribbleShots-linux-x64', 'DribbleShots');
      default:
        throw 'Unsupported platform';
    }
  }
};
