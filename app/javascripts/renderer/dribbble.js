import $ from 'jquery';
import Promise from 'promise';
import loadImage from 'image-promise';
import cardTemplate from './templates/card.handlebars';
import svgxuse from 'svgxuse';

// TODO:
// - Add transitions
// - Add basic error handling (promises)

function createDribbleUrl(page) {
  page = typeof page === 'number' ? page : 1;
  const API_KEY = '1847da24449c755dbf7f725b875dbbb5a1b714b3fcf3bcdaf8f6f64ddbab341d';
  const base = 'https://api.dribbble.com/v1/';
  const endpoint = 'teams/saturized/shots';
  const options = `?access_token=${API_KEY}&per_page=2&page=${page}`;
  const url = base + endpoint + options;

  return url;
}

function getImageUrlFromShot(shot) {
  // Get at least one image format of shot
  const image = shot.images.hidpi || shot.images.normal || shot.images.teaser;

  return image;
}

function mapShot(shot) {
  const {
    views_count: views,
    comments_count: comments,
    likes_count: likes } = shot;

  const image = getImageUrlFromShot(shot);

  return { image, views, comments, likes };
}

function renderCard(shot) {
  return new Promise(function(resolve, reject) {
    const output = cardTemplate(shot);
    if (!output) reject(); else resolve(output);
  });
}

function getDribbleShots(page) {
  return new Promise(function(resolve, reject) {
    $.get(createDribbleUrl(page)).done(function(data) {
      if (!data.length) reject();

      const shots = data.map((shot) => mapShot(shot));

      // Create promise for images
      // loadImage returns promise of img node as response
      // map shots images to loadImage promise array
      const cacheImages = Promise.all(
        shots.map((shot) => loadImage(shot.image))
      );

      cacheImages.then(function() {
        const renderedCards = Promise.all(
          shots.map((shot) => renderCard(shot))
        );

        renderedCards.then((cards) => resolve(cards));
      });
    });
  });
}

export default function app({el, page = 1, delay = 10000}) {
  // Create loader
  if (!el.children().length) el.text('loading');

  // Get shots
  getDribbleShots(page).then(function(cards) {
    const oldCards = el.find('.js-card').addClass('is-leaving');
    if (oldCards.length) {
      oldCards.on('animationend', function() {
        setTimeout(()=>el.empty().append(cards), 300);
      });
    } else {
      el.empty().append(cards);
    }

    setTimeout(() => app({el: el, page: page + 1, delay: delay}), delay);
  }).catch(function(error) {
    // Restart app on error
    setTimeout(() => app({el: el, page: 1, delay: delay}), 1000);
  });
}
